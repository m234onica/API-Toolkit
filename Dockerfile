FROM python:3.6
ADD . /usr/www/app/
COPY requirements.txt /usr/www/app/
WORKDIR /usr/www/app
RUN pip3 install -r requirements.txt
CMD python3 app.py