document.getElementById("videos").checked = true;

function checkType(obj){
    let checkLicense = document.getElementsByName('check')
    if (obj.checked) {
        if (obj.value === 'series') {
            checkLicense.forEach((element) => {
                element.disabled = true;
                element.checked = false;
            });
        }
        else if (obj.value === 'videos') {
            checkLicense.forEach((element) => {
                element.disabled = false;
            });
        }
    }
}

function checkBeforeSearch(form) {
    const videosCheck = document.getElementById("videos").checked;
    const seriesCheck = document.getElementById("series").checked;
    
    let checkFlag = false;
    const checkLicense = document.getElementsByName('check')
    
    checkLicense.forEach((element) => {
        if (element.checked) {
            checkFlag = true;
        }
    });

    if ((videosCheck && checkFlag) || seriesCheck) {
        form.submit();
    }
    else {
        if (videosCheck || seriesCheck) {
            alert("Please choose at least 1 license.");
        } else {
            alert("Please choose at least 1 type.");
        }
    }
}