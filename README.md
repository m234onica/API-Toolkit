## How to use
## Link: http://172.30.66.62:5000
### docker
```
    docker build -t sqa/video-api-toolkit:v1 .
    docker run -it -p 5000:5000 sqa/video-api-toolkit:v1
```

### normal
```
    pip3 install -r requirements.txt
    python3 app.py
```