import os, random
from flask import Flask, render_template, request, send_from_directory
from flask_bootstrap import Bootstrap
import requests

app = Flask(__name__)
bootstrap = Bootstrap(app)

license_type = [{'name': 'ppv'}, {'name': 'unlimited'}, {'name': 'freemium'}]

video_type = [{'name': 'videos'}, {'name': 'series'}]

querystring = {}

api = "https://api-videopass-cms.videopass.kkb48.com/v1/search/"


def data_clean_videos(data, elems):
    data = data['data']['videos']
    output = []
    result = []
    if elems:
        for item in data:
            if (len(elems) > 1 and item['subscription_plans']):
                if ('unlimited' in item['subscription_plans']) and item['prices']['ppv']:
                    output += [item]
            elif (elems[0] == 'unlimited' and item['subscription_plans']):
                if 'unlimited' in item['subscription_plans']:
                    output += [item]
            elif (elems[0] == 'ppv' and item['prices']['ppv']):
                output += [item]
    else:
        output = data
    for item in random.sample(output, 10):
        result += [{
            'name': item['name'],
            'id': item['id'],
            'title_id': item['title_id']
        }]

    return result


def data_clean_series(data):
    data = data['data']['series']
    result = []
    for item in random.sample(data, 10):
        name = item['series_names'][0]['name'] if item['series_names'][0]['name'] else item['name']
        result += [{
            'name': name,
            'id': item['id'],
            'title_id': item['bundle_id']
        }]

    return result

def error_handling(status_code):
	return render_template('error.html'), status_code


@app.route('/', methods=['GET'])
def input_page():
    return render_template('input.html', license=license_type, videos=video_type)


@app.route('/search', methods=['POST'])
def search():
    route = request.form.get('radio')
    license = request.form.getlist('check')

    querystring.update({'license_type': license.pop()} if license else {})
    querystring.update({'per_page': 200 if route == 'series' else 500})
    res = requests.request("GET", api + route, params=querystring)
    if res.status_code != 200:
        error_handling(res.status_code)
    try:
        res_json = res.json()
    except:
        error_handling(500)
    result = data_clean_series(res_json) if route == 'series' else data_clean_videos(res_json, license)

    return render_template('output.html', items=result)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                          'img/favicon.ico', mimetype='image/vnd.microsoft.icon')


app.run(host='0.0.0.0')